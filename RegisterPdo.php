<?php
/**
 * Created by PHP.
 * User: LuongNV
 * Date: 14-Mar-18
 * Time: 7.00 AM
 * 
 */

$servername = "localhost";
$username = "root";
$pwd = "";
try {
    $conn = new PDO("mysql:host=$servername;dbname=luongnv", $username, $pwd);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo "Không thể kết nối!" . $e->getMessage();
}
?>
<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register Form</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <?php
    $error = array();
    $data = array();
    if (isset($_POST['register'])) {
        $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
        $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
        $data['passwordConfirm'] = isset($_POST['passwordConfirm']) ? $_POST['passwordConfirm'] : '';
    // email validate 
        if ((strlen($data['email']) > 255) || (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))) {
           $error['email'] = 'Email not correct!';
    // password validate
        }
        if ((strlen($data['password']) < 6) || (strlen($data['password']) > 255)) {
            $error['password'] = 'Password not correct!';
        }
    // check confirm password
        if(empty($data['passwordConfirm'])) {
            $error['passwordConfirm'] = 'Confirm empty!';
        } elseif ($data['passwordConfirm'] != $data['password']) {
            $error['passwordConfirm'] = 'Please check password confirm!';
        }
        if (!$error) {
            $email = $_POST["email"];
            $password = $_POST["password"];
            $passwordConfirm = $_POST["passwordConfirm"];
            if ($password != $passwordConfirm) {
                header("location:RegisterPdo.php");
                setcookie("False", "Đăng nhập thất bại!", time()+2, "/","", 0);
            } else {
                $pass = md5($pass1);
                $stmt = $conn->prepare("INSERT INTO users (mail_address,password) VALUES (:email,:password)");
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':password', $password);
                $stmt->execute();
                header("location:LoginPdo.php");
                setcookie("True", "Đăng nhập thành công!", time()+2, "/","", 0);
            }
        }
    }
?>
<div class="container">
    <div id="signupbox" style="margin-top:100px; background-color: green; padding-top: 20px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div><a id="signinlink" href="LoginPdo.php?page=login">Log In</a></div>
                <div class="panel-title">
                    <?php
                    if (isset($_GET["page"]) && $_GET["page"]=="login")
                        header("location:LoginPdo.php");
                    ?>
                </div>
            </div>  
            <div class="panel-body" >
                <form method="POST" action="RegisterPdo.php" id="signupform" class="form-horizontal" role="form">   
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" placeholder="Email Address">
                        </div>
                        <b style="color: red"><?php echo isset($error['email']) ? $error['email'] : ''; ?></b>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        <b style="color: red"><?php echo isset($error['password']) ? $error['password'] : ''; ?></b>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Confirm</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="passwordConfirm" placeholder="Confirm">
                        </div>
                        <b style="color: red"><?php echo isset($error['passwordConfirm']) ? $error['passwordConfirm'] : ''; ?></b>
                    </div>
                    <div class="form-group">                                      
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="submit" name="register" class="btn btn-info"><i class="icon-hand-right"></i>Register</button>
                        </div>
                    </div>  
                </form>
                <div class="row">
                    <?php
                    if(isset($_COOKIE["False"])){
                        ?>
                        <div class="alert alert-danger">
                            <?php echo $_COOKIE["False"]; ?>
                        </div>
                        <?php } ?>
                        <?php
                        if(isset($_COOKIE["True"])){
                            ?>
                            <div class="alert alert-success">
                                <?php echo $_COOKIE["True"]; ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div> 
        </body>
        </html>