<?php 
/**
 * Created by PHP.
 * User: LuongNV
 * Date: 28-Jan-18
 * Time: 2:45 PM
 * 
 */

/**
 * [checkValidString read the file and check it is string or isn't string]
 * @param  [file] $filename [name of file will call]
 * @return [boolean]           [value True or False]
 * 
 */
function checkValidString($filename) {
//function of Exercise 1
   $str = fopen($filename, 'r') or die ("can't open the file"); 
        //open file
   $data = fread($str, filesize($filename)); 
        //read file and save into $data variable 
   if (strlen($data) == 0){ 
      return true;
  }else {
            //check 'after' doesn't exsist in $data variable 
            //check length or check 'befor' in $data variable
      if ((strpos($data, 'after') === false) && (strlen($data) >= 50 || strpos($data,'before')){ 
         return true;
     } else { 
         return false;
     }
 }
}

//Exercise 2 call function of Exercise 1
$file1 = checkValidString('file1.txt'); 
//Check file1.txt
if ($file1){
//get result of file
    echo "Chuoi hop le";
} else {
    echo "Chuoi khong hop le";
}

echo "<br>";

$file2 = checkValidString('file2.txt'); //Check file2.txt

if ($file2){ 
//get result of file
    echo "Chuoi hop le";
} else {
    echo "Chuoi khong hop le";
}
?>
