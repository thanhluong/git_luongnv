<?php 
session_start();
if (isset($_POST['Logout'])) {
    session_destroy();
    header("Location: LoginPdo.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>LoginSuccessPdo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <div class="container box" style="background-color: grey; margin-top: 50px; font-size: 250%;">  
        <h3 align="center">
            <?php
            if (isset($_SESSION['email'])) {
                echo "Log in Successfully!";
            } else {
                echo "Log in Failed!"; 
            }
            ?>
        </h3>
    </div>  
<form method="POST" action="LoginSuccessPdo.php">
    <div style="font-size: 200%;text-align: center; margin-top: -20px;">
        <input type="submit" name="Logout" value="Logout">
    </div>
</form>
</body>
</html>