@extends('layouts.default')

@section('title', 'Danh sách người dùng')

@section('content')
    <form method="GET" action="{{ route('users.index') }}" id="searchform" class="form-horizontal" style="">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="input-group col-md-12">
            <label for="email">Mail_address</label>
            <input type="text" name="mail_address" class="form-control" value="{{old('mail_address')}}">
        </div>

        <div class="input-group col-md-12">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="{{old('name')}}">
        </div>

        <div class="input-group col-md-12">
            <label for="address">Address</label>
            <input type="text" name="address" class="form-control" value="{{old('address')}}">
        </div>

        <div class="input-group col-md-12">
            <label for="phone">Phone</label>
            <input type="text" name="phone" class="form-control" value="{{old('phone')}}">
        </div>

        <button type="submit" name="search">Tìm kiếm</button>
    </form>
    <table class="table table-bordered">
        <div class="container">
            @include('flash::message')
        </div>
        <tr class="success">
            <th><p align="center">STT</p></th>
            <th><p align="center">Địa chỉ email</p></th>
            <th><p align="center">Tên</p></th>
            <th><p align="center">Địa chỉ</p></th>
            <th><p align="center">Số điện thoại</p></th>
        </tr>
        @php 
            $i = ($users->currentpage() - 1) * $users->perpage() + 1;
        @endphp
        @foreach ($users as $user)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $user->mail_address }}</td>
                <td>{{ Helper::toUpperCase($user->name) }}</td>
                <td>{{ $user->address }}</td>
                <td>{{ $user->phone }}</td>
            </tr>
        @endforeach
    </table>
    <form action="{{ route('users.create') }}">
        <div class="rows">
            <div>
                {{ $users->links() }}
                <button style="float: right;color: red" type="submit">Đăng Ký</button>
            </div>
        </div>
    </form>
@endsection
