@extends('layouts.default')

@section('title', 'Thêm mới người dùng')

@section('content')
    <div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="panel-body" >
                    <form method="POST" action="{{ route('users.store') }}" id="loginform" class="form-horizontal" role="form">
                        <div class="container">
                            @include('flash::message')
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_field() }}">
                        <div class="form-group @if($errors->has('mail_address')) has-error @endif">
                            <label for="email">Email</label>
                                <input type="text" class="form-control" name="mail_address" value="{{old('mail_address')}}">
                                @if($errors->has('mail_address'))
                                <span class="text text-danger">{{ $errors->first('mail_address') }}</span>
                                @endif
                        </div>

                        <div class="form-group @if($errors->has('password')) has-error @endif" >
                            <label for="password">Password:</label>
                                <input type="password" class="form-control" name="password" value="{{old('password')}}">
                                @if($errors->has('password'))
                                <span class="text text-danger">{{ $errors->first('password') }}</span>
                                @endif
                        </div>

                        <div class="form-group @if($errors->has('password_confirmation')) has-error @endif">
                            <label for="password">Password_confirmation:</label>
                                <input type="password" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
                                @if($errors->has('password_confirmation'))
                                <span class="text text-danger">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                        </div>

                        <div class="form-group @if($errors->has('name')) has-error @endif">
                            <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                @if($errors->has('name'))
                                <span class="text text-danger">{{ $errors->first('name') }}</span>
                                @endif
                        </div>

                        <div class="form-group @if($errors->has('address')) has-error @endif" >
                            <label for="address">Address:</label>
                                <input type="text" class="form-control" name="address" value="{{old('address')}}">
                                @if($errors->has('address'))
                                <span class="text text-danger">{{ $errors->first('address') }}</span>
                                @endif
                        </div>

                        <div class="form-group @if($errors->has('phone')) has-error @endif">
                            <label for="phone">Phone:</label>
                                <input type="text" class="form-control" name="phone" value="{{old('phone')}}">
                                @if($errors->has('phone'))
                                <span class="text text-danger">{{ $errors->first('phone') }}</span>
                                @endif
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 control">
                                <button type="submit" class="text text-danger">Register</button>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

