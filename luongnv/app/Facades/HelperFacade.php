<?php

namespace App\Facades;

use App\Models\User;
use Illuminate\Support\Facades\Facade;

class HelperFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'helperFacede';
    }
}
