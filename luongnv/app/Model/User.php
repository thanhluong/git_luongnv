<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'users';
    protected $fillable = ['mail_address', 'password', 'name', 'address', 'phone'];
    protected $perPage = 20;

    /**
     * Get list user from database.
     *
     * @param  \Illuminate\Http\Request  $builder
     * @return \Illuminate\Http\Response
     */
    public function getUsers(array $data)
    {
        $builder = User::orderBy('mail_address', 'ASC');
            if (isset($data['mail_address'])) {
                $builder->where('mail_address', 'LIKE', '%' . $data['mail_address'] . '%');
            }

            if (isset($data['name'])) {
                $builder->where('name', 'LIKE', '%' . $data['name'] . '%');
            }

            if (isset($data['address'])) {
                $builder->where('address', 'LIKE', '%' . $data['address'] . '%');
            }

            if (isset($data['phone'])) {
                $builder->where('phone', '%' . $data['phone'] . '%');
            }
        return $builder->paginate();
    }

    /**
     * Insert user to database.
     *
     * @param  \Illuminate\Http\Request  $data
     * @return \Illuminate\Http\Response
     */
    public function createUser(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        return User::create($data);
    }
}
