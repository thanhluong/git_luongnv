<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use Illuminate\Validation\Validator;

class UsersController extends Controller
{
    protected $modelUser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(User $modelUser)
    {
        $this->modelUser = $modelUser;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = $this->modelUser->getUsers($request->all());
        return view('users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $users = $this->modelUser->createUser($request->all());
        flash('Thêm người dùng mới thành công')->success()->important();
        return redirect()->route('users.index');
    }
}
