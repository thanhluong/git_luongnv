<?php 
/**
 * Created by PHP.
 * User: LuongNV
 * Date: 5-Feb-18
 * Time: 7.00 AM
 * 
 */

//Create Interface and abstract class
Interface Boss
{
    public function checkValidSlogan();
}

abstract class Supervisor implements Boss
{
    public $logan;
    
    public function saySloganOutLoud()
    {
        echo $this->slogan;
    }

    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }
}

//Create trait using in Bonus exercise
trait Active
{
    public function defindYourSelf() 
    {
        return get_class($this);
    }
}

//Create subclass EasyBoss 
class EasyBoss extends Supervisor 
{
    use Active;
	
    public function checkValidSlogan() 
    {
        $variable1 = false;
        if ((strpos($this->slogan, 'after') === false) && (strpos($this->slogan, 'before') === false)) {
            $variable1 = false;
        } else {
            $variable1 = true;
        }
        return $variable1;
    }
}

//Create subclass UglyBoss 
class UglyBoss extends Supervisor 
{
    use Active;
	
    public function checkValidSlogan() 
    {
        $variable2 = false;
        if ((strpos($this->slogan, 'after') === false) || (strpos($this->slogan, 'before') === false)) {
            $variable2 = flase;
        } else {
            $variable2 = true;
        }
        return $variable2;
    }
}

//Create object of EasyBoss class and UglyBoss class
$easyBoss = new EasyBoss();
$uglyBoss = new UglyBoss();
//Input string using object created
$easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');
$uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');
$easyBoss->saySloganOutLoud(); 
echo "<br>";
$uglyBoss->saySloganOutLoud(); 

echo "<br>";
echo "<br>";

var_dump($easyBoss->checkValidSlogan()); 
echo "<br>";
var_dump($uglyBoss->checkValidSlogan()); 

echo "<br>";
echo "<br>";

// Using trait created at above
echo 'I am ' . $easyBoss->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $uglyBoss->defindYourSelf(); 

