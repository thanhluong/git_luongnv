<?php
$servername = "localhost";
$username = "root";
$pwd = "";
try {
    $conn = new PDO("mysql:host=$servername;dbname=luongnv", $username, $pwd);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo "Không thể kết nối!" . $e->getMessage();
}
?>
<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <?php
    $error = array();
    $data = array();
    if (isset($_POST['login'])) {
        $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
        $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
    // email validate 
        if ((strlen($data['email']) > 255) || (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))) {
         $error['email'] = 'Email not correct!';
     }
    // password validate
     if ((strlen($data['password']) < 6) || (strlen($data['password']) > 255)) {
       $error['password'] = 'Password not correct!';
   }
    //check email in database
   if (!$error) {
    $email = $_POST["email"];
    $password = $_POST["password"];
    $rows = $conn->prepare("SELECT mail_address,password FROM users WHERE mail_address = :email AND password = :password");
    $rows->bindParam(':email', $email);
    $rows->bindParam(':password', $password);
    $rows->execute();
    $results = $rows->fetchAll(PDO::FETCH_ASSOC); 
    if (count($results) != 0) {
        $_SESSION['email'] = $email;
        if (isset($_POST["remember"])) {
            setcookie("member_login", $_POST['email'], time()+ (24 * 60 * 60));  
            setcookie("member_password", $_POST['password'], time()+ (24 * 60 * 60));  
        }
        header("location:LoginSuccessPdo.php");
    } else {
        $error['rememberme'] = 'Đăng nhập thất bại!';
             // setcookie("False", "Đăng nhập thất bại!", time()+2, "/","", 0);
    }
}
}
?>
<div class="container">    
    <div id="loginbox" style="margin-top:50px; background-color: grey; padding-top: 25px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Log In</div>
            </div>     
            <div style="padding-top:30px" class="panel-body" >                            
                <form method="POST" action="LoginPdo.php" id="loginform" class="form-horizontal" role="form">
                    <b style="color: red"><?php echo isset($error['email']) ? $error['email'] : ''; ?></b>       
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-email" type="text" class="form-control" name="email" value="<?php if(isset($_COOKIE["member_login"]))?>"" placeholder="Email">     
                    </div>
                    <b style="color: red"><?php echo isset($error['password']) ? $error['password'] : ''; ?></b>   
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" value="<?php if(isset($_COOKIE["member_password"])) ?>" placeholder="password">
                    </div>
                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <input id="login-remember" type="checkbox" name="remember"> Remember me
                            </label>
                        </div>
                    </div>
                    <div style="margin-top:10px" class="form-group">
                        <div class="col-sm-12 controls">
                            <input type="submit" name="login" id="btn-login" href="#" class="btn btn-success" value="Login">
                        </div>
                        <b style="color: red"><?php echo isset($error['rememberme']) ? $error['rememberme'] : ''; ?></b>      
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 3px solid#888; padding-top:15px; font-size:85%; text-align: right;" >
                                <a href="LoginPdo.php?page=register" class="btn btn-success">Register</a>
                            </div>
                            <div class="col-md-12 control">
                                <?php
                                if (isset($_GET["page"])&&$_GET["page"]=="register")
                                    header("location:RegisterPdo.php");
                                ?>
                            </div>
                        </div>
                    </div>    
                </form> 
            </div>                     
        </div>  
    </div>
</div>
</body>
</html>