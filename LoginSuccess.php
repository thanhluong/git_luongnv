<?php
/**
 * Created by PHP.
 * User: LuongNV
 * Date: 9-Feb-18
 * Time: 7.00 AM
 * 
 */

session_start();
if (isset($_POST['logout'])) {
  session_destroy();
  header("Location: Login.php");
}
?>

<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title></title>
</head>
<body>
  <h1>LoginSuccess</h1>
  <div>
    <?php 
    if (isset($_SESSION['name'])) {
        echo 'Đăng nhập thành công';
    } else { 
        $_SESSION['error'] = 'Bạn cần đăng nhập lại để vào trang LoginSucess'; 
        header("Location: Login.php");
    }
    ?>
</div>
<br>
<form method="POST" action="LoginSuccess.php">
    <input type="submit" name="logout" value=" <?php if (isset($_SESSION['name'])) {echo 'Logout';} else {echo 'Again';}?>" />
</form>
</body>
</html>
