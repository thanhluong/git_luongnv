<?php
/**
 * Created by PHP.
 * User: LuongNV
 * Date: 9-Feb-18
 * Time: 7.00 AM
 * 
 */

session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
    <h1>Login</h1>
    <p style="color: red" ><b><?= (isset($_SESSION['error'])) ? $_SESSION['error'] :'' ?> </b></p>
    <?php
    $error = array();
    $data = array();
    if (!empty($_POST['login'])) {
    // Get data into variable
        $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
        $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';

        function is_email($email)
        {
            return (!filter_var($email, FILTER_VALIDATE_EMAIL)) ? FALSE : TRUE;
        }

        function is_password($password)
        {
            return (!preg_match("/^[a-zA-Z1-9 ]*$/", $password)) ? FALSE : TRUE;
        }

        if (empty($data['email'])) {
            $error['email'] = 'Email empty';
        } elseif (!is_email($data['email'])) {
            $error['email'] = 'Email invalid';
        } elseif ($data['email'] != 'luongnv@gmail.com') {
            $error['email'] = ' * Email not correct';
        }

        if (empty($data['password'])) {
            $error['password'] = 'Password empty';
        } elseif (!is_password($data['password'])) {
            $error['password'] = 'Password invalid';
        } elseif ($data['password'] != 'luongnv123') {
            $error['password'] = ' * Password not correct';
        } 

    // Save data
        if (!$error) {
            if (isset($_POST['remember'])) {
                setcookie('email_login', $_POST['email'], time() + (24 * 60 * 60));
                setcookie('password', $_POST['password'], time() + (24 * 60 * 60));
            }
            $_SESSION['name'] = 'luongnv@gmail.com';
            header("Location: LoginSuccess.php");
        } else {
            echo 'Đăng nhập thất bại';
        }
    }
    ?>
    <label style="color: red" ><b> <?php echo isset($error['error']) ? $error['error'] : ''; ?>  </b></label>
    <form method="POST" action="Login.php">
        <div class="container">
            <label><b>Email</b> </label>
            <p style="color: red" ><b> <?php echo isset($error['email']) ? $error['email'] : ''; ?>  </b></p>
            <input type="email" name="email" value="<?= isset($_COOKIE['email_login']) ? $_COOKIE['email_login'] : ' ' ?>" />
            <br>
            <br>
            <label><b>Password</b> </label>
            <p style="color: red" ><b> <?php echo isset($error['password']) ? $error['password'] : ''; ?>  </b></p>
            <input type="password" name="password" value="<?= isset($_COOKIE['password']) ? $_COOKIE['password'] : ' ' ?>" />
            <br>
            <br>
            <input type="submit" name="login" value="Login" />
            <br>
            <br>
            <label><input type="checkbox" checked="checked" name="remember" /> Remember me </label>
        </div>

    </div>
</form>
</body>
</html>
